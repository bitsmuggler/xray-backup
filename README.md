# Backup Xray Cloud

These shell scripts help you to run a backup of Xray for Jira (Cloud). It contains the following files:

* cloud_auth.json
    * Add your generated client_id and client_credential
* backup.sh
    * Starts a backup job on xray
* download.sh
    * Downloads backup        
## Preparing
1. Create API key (client_id & client_credentials) under Jira Settings / Apps / API Keys "Create API Keys"
2. Set the credentials in the `cloud_auth.json`file
3. Set execution rights `chmod +x backup.sh` and `chmod +x download.sh`
## Run the scripts

1. `./backup.sh`
2. `./download.sh`(after the backup has completed)

## Documentation

* [Xray API Documentation](https://docs.getxray.app/display/XRAYCLOUD/Backup+-+REST)