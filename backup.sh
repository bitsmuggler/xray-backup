#!/bin/bash

token=$(curl --location --request POST 'https://xray.cloud.xpand-it.com/api/v1/authenticate' \
--header 'Content-Type: application/json' --data @"cloud_auth.json" ) 

token="${token//\"}"

curl --location --request POST 'https://xray.cloud.xpand-it.com/api/v1/backup' \
--header "Authorization: Bearer $token" \
--header 'Content-Type: application/json' \