#!/bin/bash

token=$(curl --location --request POST 'https://xray.cloud.xpand-it.com/api/v1/authenticate' \
--header 'Content-Type: application/json' --data @"cloud_auth.json" ) 

token="${token//\"}"

curl -H "Content-Type: application/json" -X GET -H "Authorization: Bearer $token" https://xray.cloud.xpand-it.com/api/v1/backup/file --output './backup.zip'